def testApp() {
    echo "##### Running tests #####"
    sh 'mvn test'
}

def buildJar() {
    echo "##### Building JAR Artifact ######"
    sh "mvn clean package -DskipTests"
}

def dockerLogin() {
    echo "##### Login to private DockerHub #####"
    withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
        sh 'echo $PASSWORD | docker login -u $USERNAME --password-stdin'
    }
    
}

def buildDockerImage(String IMAGE_NAME) {
    echo "##### Building image #####"
    sh "docker build -t ${IMAGE_NAME} ."
}


def dockerPush(String IMAGE_NAME) {
    echo "##### Push to private DockerHub #####"
    sh "docker push ${IMAGE_NAME}"
}

return this