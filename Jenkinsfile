#!/usr/bin/env groovy

def gv

pipeline {
    agent any
    tools {
        maven 'maven-3.8'
    }

stages {
        stage('Init') {
            steps {
               script {
                  echo 'Init Stage'
                    gv = load "script.groovy"                  
               }
            }
        }
        stage("Incrementing App Version") {
            steps {
                script {
                    echo "Incrementing App Version"
                    sh "mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit"
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "glarez/aws-java-maven-demo:$version-$BUILD_NUMBER"
                }
            }
        }

        stage('Test') {
            steps {
               script {
                  echo "Testing branch $BRANCH_NAME"
                  gv.testApp()                
               }
            }
        }
        stage('Build Artifact') {
            steps {
               script {
                  echo "building application jar from branch $BRANCH_NAME"
                  gv.buildJar()
               }
            }
        }
        stage('Processing Docker Image') {
            steps {
                script {
                   echo "Working on docker image ${IMAGE_NAME}"
                   gv.dockerLogin()
                   gv.buildDockerImage(env.IMAGE_NAME)
                   gv.dockerPush(env.IMAGE_NAME)
                }
            }
        }
        stage('Deploy') {
            steps {
                script {
                   echo "Deploying docker image ${IMAGE_NAME} to EC2..."
                   def ec2Instance = "ec2-user@54.87.89.208"
                   def shellCMD = "bash ./server-cmds.sh ${IMAGE_NAME}"
                   sshagent(['aws-ec2-creds']) {
                       sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                       sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                       sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCMD}" // to be used with shell script
                   }
                }
            }
        }
        stage("Commit Version Update") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'jenkinsIncrementVerToken', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                        sh 'git config --global user.email jenkins@glarez.com'
                        sh 'git config --global user.name jenkins'
                        sh 'git status'
                        sh 'git branch'
                        sh 'git config --list'
                        sh 'git remote set-url origin https://${USERNAME}:${PASSWORD}@gitlab.com/germanlarez/aws-jenkins2.git'
                        sh 'git add .'
                        sh 'git commit -m "Jenkins CI version bump in pom.xml to version ${IMAGE_NAME}"'
                        sh 'git push origin HEAD:master'
                    }
                }
            }
        }
    }   
}